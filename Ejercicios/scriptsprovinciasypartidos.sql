use db_candela;
create table PROVINCIAS(
PROV_ID integer NOT NULL PRIMARY KEY(PROV_ID) IDENTITY,
PROV_DESCRIPTION VARCHAR(50)
);

create table PARTIDOS(
PAR_ID integer NOT NULL PRIMARY KEY(PAR_ID) IDENTITY,
PROV_ID integer FOREIGN KEY REFERENCES PROVINCIAS(PROV_ID),
PAR_DESCRIPTION VARCHAR(50)
);
